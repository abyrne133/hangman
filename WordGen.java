/*
WordGen.java -- An instantiable class with a method to return one random word from a list of 40 words.
@author Alan Byrne
studentID x15030971
Alan.Byrne2@student.ncirl.ie
@date 17/03/2016
*/

public class WordGen{
	private String randomWord;
	private String[] words;

	public WordGen(){
			words = new String[40];
			words[0] = "programming";
			words[1] = "violincello";
			words[2] = "selection";
			words[3] = "repetition";
			words[4] = "serendipity";
			words[5] = "performance";
			words[6] = "computing";
			words[7] = "paint";
			words[8] = "passion";
			words[9] = "contemporary";
			words[10] = "ceramic";
			words[11] = "laboratory";
			words[12] = "production";
			words[13] = "formulating";
			words[14] = "quality";
			words[15] = "colours";
			words[16] = "blue";
			words[17] = "marketing";
			words[18] = "music";
			words[19] = "football";
			words[20] = "technology";
			words[21] = "guitar";
			words[22] = "piano";
			words[23] = "communication";
			words[24] = "relationship";
			words[25] = "keyboard";
			words[26] = "mouse";
			words[27] = "television";
			words[28] = "project";
			words[29] = "business";
			words[30] = "money";
			words[31] = "building";
			words[32] = "street";
			words[33] = "space";
			words[34] = "spade";
			words[35] = "brother";
			words[36] = "sister";
			words[37] = "orchestra";
			words[38] = "trousers";
			words[39] = "amplifier";
	}

	//a getter method to return the random word - also includes the processing to choose the random word
	public String getRandomWord(){
		randomWord = words[(int)(Math.random()*40)];//choose a random integer between zero and 40, use this number as the an index to select a word from the words array
		return randomWord;
	}

	public int getSize(){
		return words.length;
	}
}