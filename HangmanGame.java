/*
HangmanGame.java -- The engine used to run the hangman game
@author Alan Byrne
studentID x15030971
Alan.Byrne2@student.ncirl.ie
@date 16/03/2016
*/

import java.util.Scanner;
public class HangmanGame{
	public static void main(String args[]){
		//declare variables
		String answer;
		String playAgain;
		int lives;
		int gamesPlayed;
		int gamesWon;
		Boolean snap;

		//create instances of objects
		StringBuffer randomWords = new StringBuffer(); // for the random words generated in the gaming session
		Scanner input = new Scanner(System.in); // used anytime user input is needed
		WordGen random = new WordGen();//runs the constructor to populate the String array

		//gaming session
		gamesPlayed = 0;
		gamesWon = 0;

		do{
			lives = 8;

			do{//this loop ensures the same random word is not used twice in one session
				answer = random.getRandomWord();
			}while(((randomWords.toString()).toLowerCase()).contains(answer.toLowerCase())); // 'contains' method from stackoverflow.com
			randomWords.append(answer + " "); // add the random word to already used random words, if any

			Hangman game = new Hangman(answer,lives);
			do{
				game.setLetter();
				snap = game.checkMatch(); //check for letter and word match, return true if word is matched and assign to snap, also prints relevant feedback to user
				lives = game.getLives();
			}while (lives != 0 && !snap);

			if(lives == 0){
				System.out.println("You lost all your lives!");
				System.out.println("The answer was: " + answer);
			}else if (snap){
				System.out.println("Word: " + answer);
				System.out.println("Congratulations, you won!");
				gamesWon++;
			}

			//ask the user to play again
			System.out.println("To play again enter 'y' otherwise type anything else:");
			playAgain = input.next();
			playAgain = playAgain.trim(); //remove possible white space around user input, oracle website used to get 'trim' method
			gamesPlayed++;
		}while(playAgain.equalsIgnoreCase("Y") && gamesPlayed < random.getSize()); // play the game at least once (do...while loop) then allow the user to choose if they play again, up to 40 times (one time for each word)

		//session stats
		System.out.println();
		System.out.println("Game stats:");
		System.out.println("Games Played: " + gamesPlayed);
		System.out.println("Games Won: " + gamesWon);
		System.out.println("Games Lost: " + (gamesPlayed - gamesWon));
	}
}