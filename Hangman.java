/*
Hangman.java -- An instantiable class for each hangman game.
@author Alan Byrne
studentID x15030971
Alan.Byrne2@student.ncirl.ie
@date 18/03/2016
*/

import java.util.Scanner;
public class Hangman{
	//declare instance variables
	private String answer;
	private String letter;
	private int lives;
	private boolean snapLetter;
	private boolean snapWord;
	private boolean isLetter;
	private boolean isUsed;

	//create instances of objects
	Scanner input = new Scanner(System.in);
	StringBuffer usedLetters = new StringBuffer();
	private StringBuffer word = new StringBuffer();
	private StringBuffer displayWord = new StringBuffer();

	//constructor
	public Hangman(String answer, int lives){
		this.answer = answer;
		this.lives = lives;

		for(int i = 0; i < answer.length(); i++){
		word.append("_");
		displayWord.append("_ ");
		}

		System.out.println();
		System.out.println("Lives: " + lives);
		System.out.println("Word: " + displayWord);
	}

	public int getLives(){
		return lives;
	}

	private String getWord(){
		displayWord.replace(0,displayWord.length(),word.toString());
		for(int i=0; i < word.length();i++){
			displayWord.insert(2*i+1," ");//adds a space after each letter in the word (and some space to the end of the word)
			}
		return displayWord.toString();
	}

	public void setLetter(){

		//validate the user input
		System.out.println("Guess a letter:");
		letter = (input.next()).trim();

		isLetter = (letter.length()== 1 && Character.isLetter(letter.charAt(0)));
		isUsed = ((usedLetters.toString()).toLowerCase()).contains(letter.toLowerCase());

		while(!isLetter || isUsed){ //not a letter or a previously used letter
			System.out.println("You did not enter a letter or you re-used a letter, please try again:");
			letter = (input.next()).trim();
			isLetter = (letter.length()== 1 && Character.isLetter(letter.charAt(0)));//check if input is a letter
			isUsed = ((usedLetters.toString()).toLowerCase()).contains(letter.toLowerCase());
		}

		if (usedLetters.length() == 0){//adds the letter to the record after the above validation
			usedLetters.append(letter);
		}else {
			usedLetters.append(", " + letter );
		}
	}

	public boolean checkMatch(){
		snapLetter = false;//reset to false before each check of a letter
		for(int i = 0; i < answer.length(); i++){
			if(letter.charAt(0) == answer.charAt(i)){
				snapLetter = true;
				word.replace(i,i+1,letter);
			}
		}

		if(!snapLetter){
			lives--;
		}

		snapWord = (word.toString()).equalsIgnoreCase(answer); //convert Stringbuffer word to string before using the string method equalsIgnoreCase()
		System.out.println();
		if (lives != 0 && !snapWord){ //dont print this output if the user has just won or lost the game
		System.out.println("Lives: " + lives);
		System.out.println("Word: " + getWord());
		System.out.println("Used Letters: " + usedLetters);
		}
		return snapWord;
	}
}